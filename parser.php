<?php
	class Parser 
	{
        private $date_text;

		/**
		 * Constructor
		 * 
		 * @param $argv Command line arguments
		 */
		function __construct($argv) {
			if(isset($argv[1])) {
				$file = file_get_contents($argv[1]);
		
				if(!$file) {
					throw new InvalidArgumentException("Please provide a valid file path");
					exit();
				}

				$git_log_json = $this->convertToJson($file);
			} else if($log = stream_get_contents(STDIN)) {
				$git_log_json = $this->convertToJson($log);
			}

			$fp = fopen(__DIR__ . "/gitlog.json", "w");
			fputs($fp, $git_log_json, strlen($git_log_json));
			fclose($fp);
		}

		/**
		 * Convert Git log to Json
		 * 
		 * @param $file Git log
		 * @return Formatted message
		 */
		function convertToJson($file) {
			$file = str_replace(PHP_EOL, ' \n ', $file);
			$commits = preg_split("/(?=commit \w{40} \\\\n)/", $file);

			$parsed_commits = [];
			foreach($commits as $commit) {
				if($commit !== "") {
					$this->date_text = '';
					
					$commit_details = [];
                    $commit_details['commit'] = $this->getCommitId($commit);
                    $this->getMerge($commit) !== false ? $commit_details['merge'] = $this->getMerge($commit) : false;
                    $commit_details['author'] = $this->getAuthor($commit);
					$commit_details['date'] = $this->getDate($commit);
					$commit_details['message'] = $this->getMessage($commit);

					$parsed_commits[] = $commit_details;
				}
			}

			return json_encode($parsed_commits);
        }
		
		/**
		 * Get commit ID
		 * 
		 * @param $commit Commit
		 * @return ID
		 */
        function getCommitId($commit) {
            preg_match("/commit \w{40}/", $commit, $id);
            if(isset($id[0])) {
                return explode(' ', $id[0])[1];
            }
        }

		/**
		 * Get merge ID's
		 * 
		 * @param $commit Commit
		 * @return Commit ID's or false
		 */
        function getMerge($commit) {
            preg_match("/Merge: \w{8} \w{8}/", $commit, $merge_id);
            if(isset($merge_id[0])) {
                return [
                    'id1' => explode(' ', $merge_id[0])[1],
                    'id2' => explode(' ', $merge_id[0])[2]
                ];   
            } else {
                return false;
            }
        }

		/**
		 * Get commit author
		 * 
		 * @param $commit Commit
		 * @return Author name and email
		 */
        function getAuthor($commit) {
            preg_match("/Author: .+? (?=\\\\n)/", $commit, $author);
            if(isset($author[0])) {
                return trim(substr($author[0], 8, strlen($author[0])));
            }
        }

		/**
		 * Get commit date
		 * 
		 * @param $commit Commit
		 * @return Formatted date
		 */
        function getDate($commit) {
            preg_match("/Date: .+? (?=\\\\n)/", $commit, $date);
            if(isset($date[0])) {
                $this->date_text = trim(substr($date[0], 6, strlen($date[0])));
                $date = new DateTime($this->date_text);
                return $date->format('d-m-Y H:i:s');
            }
        }

		/**
		 * Get message from commit
		 * 
		 * @param $commit Commit
		 * @return Formatted message
		 */
        function getMessage($commit) {
            $date_index = strpos($commit, $this->date_text);
            $message = substr($commit, ($date_index + strlen($this->date_text)), strlen($commit));
            return trim(str_replace(' \n ', PHP_EOL, $message));
        }
	}
	
	new Parser($argv);
?>