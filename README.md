# Git log parser

Command to run is:

`git log | php ../git-log-parser/parser.php`


If the git log is already saved in a file, use:

`php parser.php PATH`